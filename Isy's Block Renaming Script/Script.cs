// Block Renaming Script by Isy
// =======================
// Version: 1.4.3
// Date: 2018-08-16

// =======================================================================================
//                                                                            --- Configuration ---
// =======================================================================================

// Define the character that will be added between names and numbers when sorting or adding strings
// Default (whitespace): char spaceCharacter = ' ';
const char spaceCharacter = ' ';

// Keyword for sorting
const string sortKeyword = "!sort";

// Keyword for test mode
const string testKeyword = "!test";

// Keyword for help:
const string helpKeyword = "!help";

// =======================================================================================
//                                                                            --- Instructions ---
// =======================================================================================

/*

		Run the script with one of the following arguments:
		(Parameters are always divided with a comma ',')

		--- Rename ---
		Blocks are renamed to NEWNAME if the blockname contains BLOCKFILTER.
		rename,BLOCKFILTER,NEWNAME

		--- Replace ---
		Replace a string with another one.
		replace,OLDSTRING,NEWSTRING

		--- Remove ---
		Remove a string from your blocknames.
		The parameter BLOCKFILTER is optional and filters the used blocks! All blocks that match the search are renamed if it's missing:
		remove,STRING[,BLOCKFILTER]

		--- Remove Numbers ---
		Remove numbers from your blocknames.
		The parameter BLOCKFILTER is optional and filters the used blocks! All blocknumbers are removed if it's missing:
		removenumbers[,BLOCKFILTER]

		--- Sort ---
		Create new continuous numbers for a list of blocks defined by BLOCKFILTER (e.g. Refinery):
		sort,BLOCKFILTER

		Same as above but gives gives continuous numbers based on the grid:
		sortbygrid,BLOCKFILTER

		--- Autosort ---
		Autosort all blocks on the current grid:
		autosort
		Autosort all blocks on a specific grid:
		autosort,GRIDNAME
		Autosort all blocks on every connected grid:
		autosort,all

		--- Add strings ---
		Add a string at the front or back of your blocknames.
		The parameter BLOCKFILTER is optional and filters the used blocks! All blocks are renamed if it's missing:
		addfront,STRING[,BLOCKFILTER]
		addback,STRING[,BLOCKFILTER]

		--- Add strings whole grid ---
		Add a string at the front or back of all blocks on a grid (partial gridnames are also detected):
		addfrontgrid,STRING,GRIDNAME
		addbackgrid,STRING,GRIDNAME

		--- Rename a grid ---
		The parameter BLOCKONGRID is optional and is only needed to identify a subgrid. The grid of the PB is renamed if it's missing:
		renamegrid,NEWNAME[,BLOCKONGRID]

		--- Sort mode ---
		Sortmode can be applied to every operation. It sorts the current selection after renaming.
		Add this parameter at the end of your argument, e.g.: "replace,OLDSTRING,NEWSTRING,!sort"
		!sort

		--- Test mode ---
		Testmode shows what is going to happen, without actually renaming anything.
		Add this parameter at the end of your argument, e.g.: "replace,OLDSTRING,NEWSTRING,!test"
		!test

		*/


// =======================================================================================
//                                                                      --- End of Instructions ---
//                                                        Don't change anything beyond this point!
// =======================================================================================


// Global variables
bool testMode, sortMode, showHelp;
string pattern = @"( |" + spaceCharacter + @")\d+\b";
string title = "Isy's Block Renaming Script\n======================\n";
List<string> sortList = new List<string>();
List<string> result = new List<string>();

// Autosort variables
bool continueAutosort = false;
string autosortFilter = "";
int autosortCounter = 0;
int gridIndex = 0;

// Show help after compile
public Program()
{
	result.Add(title);
	Help();
}

// Main
public void Main(string args)
{
	if (continueAutosort) {
		continueAutosort = false;
		int counter = Autosort(autosortFilter, autosortCounter);

		if (continueAutosort) {
			return;
		} else {
			autosortFilter = "";
			autosortCounter = 0;
			gridIndex = 0;

			ShowResult(counter);
			return;
		}
	}


	// Format the parameter
	List<string> param = args.Split(',').ToList();

	testMode = false;
	sortMode = false;
	showHelp = false;

	sortList.Clear();
	result.Clear();

	if (args.Contains(testKeyword)) {
		testMode = true;
		param.Remove(testKeyword);
	}
	if (args.Contains(sortKeyword)) {
		sortMode = true;
		param.Remove(sortKeyword);
	}
	if (args.Contains(helpKeyword)) {
		showHelp = true;
		param.Remove(helpKeyword);
	}

	result.Add(title);
	int count = 0;

	if (param.Count == 0) {
		Help();
		ShowResult(count);
		return;
	}

	if (args == String.Empty) {
		// Help
		Help();

	} else if (showHelp) {
		// Show help for a specific topic
		Help(param[0]);

	} else if (param[0] == "rename") {
		// Rename
		if (param.Count >= 3) {
			count = Rename(param[1], param[2]);
		} else {
			result.Add("Error!\n'" + param[0] + "' needs three parameters!\n");
			Help(param[0]);
		}

	} else if (param[0] == "replace") {
		// Replace
		if (param.Count >= 3) {
			count = Replace(param[1], param[2]);
		} else {
			result.Add("Error!\n'" + param[0] + "' needs three parameters!\n");
			Help(param[0]);
		}

	} else if (param[0] == "remove") {
		// Remove
		if (param.Count == 2) {
			count = Remove(param[1]);
		} else if (param.Count >= 3) {
			count = Remove(param[1], param[2]);
		} else {
			result.Add("Error!\n'" + param[0] + "' needs at least two parameters!\n");
			Help(param[0]);
		}

	} else if (param[0] == "removenumbers") {
		// Remove numbers
		if (param.Count == 1) {
			count = RemoveNumbers();
		} else if (param.Count >= 2) {
			count = RemoveNumbers(param[1]);
		}

	} else if (param[0] == "sort") {
		if (param.Count >= 2) {
			count = Sort(param[1]);
		} else {
			result.Add("Error!\n'" + param[0] + "' needs two parameters!\n");
			Help(param[0]);
		}

	} else if (param[0] == "sortbygrid") {
		if (param.Count >= 2) {
			count = Sortbygrid(param[1]);
		} else {
			result.Add("Error!\n'" + param[0] + "' needs two parameters!\n");
			Help("sort");
		}

	} else if (param[0] == "autosort") {
		// Autosort
		if (param.Count == 1) {
			count = Autosort();
		} else if (param.Count >= 2) {
			count = Autosort(param[1]);
		}
		if (continueAutosort) return;

	} else if (param[0] == "addfront" || param[0] == "addback") {
		// Add a string at front or back
		if (param.Count == 2) {
			count = Add(param[0], param[1]);
		} else if (param.Count >= 3) {
			count = Add(param[0], param[1], param[2]);
		} else {
			result.Add("Error!\n'" + param[0] + "' needs at least two parameters!\n");
			Help(param[0]);
		}

	} else if (param[0] == "addfrontgrid" || param[0] == "addbackgrid") {
		// Add a string at front or back on a grid
		if (param.Count >= 3) {
			count = Add(param[0], param[1], param[2]);
		} else {
			result.Add("Error!\n'" + param[0] + "' needs three parameters!\n");
			Help(param[0]);
		}

	} else if (param[0] == "renamegrid") {
		// Rename a grid
		if (param.Count == 2) {
			count = RenameGrid(param[1]);
		} else if (param.Count >= 3) {
			count = RenameGrid(param[1], param[2]);
		} else {
			result.Add("Error!\n'" + param[0] + "' needs at least two parameters!\n");
			Help(param[0]);
		}

	} else {
		result.Add("Error!\nUnknown Command!\n");
		Help();
	}

	// Autosort all if set
	if (sortMode) {
		result.Add("\nSort mode:\n=========\n");
		SortMode(sortList);
	}

	// Give a final message
	if (param[0] == "renamegrid") {
		ShowResult(count, true);
	} else {
		ShowResult(count);
	}
}


// Rename
int Rename(string oldName, string newName)
{
	// Create a new list and get all the blocks that match the name
	var blocks = new List<IMyTerminalBlock>();
	GridTerminalSystem.GetBlocksOfType<IMyTerminalBlock>(blocks, b => b.CustomName.Contains(oldName));

	int counter = 0;

	// Cycle through all the blocks
	foreach (var block in blocks) {
		counter++;

		// Create a new name string
		string blockName = block.CustomName;
		string number = GetNumberStr(blockName);
		string newBlockName = newName + number;

		sortList.Add(newBlockName);

		result.Add(counter + ". " + blockName);
		result.Add("   -> " + newBlockName);

		if (!testMode) block.CustomName = newBlockName;
	}

	return counter;
}


// Replace
int Replace(string oldString, string newString, string filter = "")
{
	// Create a new list and either get all the blocks or the ones, that match the name
	List<IMyTerminalBlock> blocks = new List<IMyTerminalBlock>();
	if (filter != "") {
		GridTerminalSystem.GetBlocksOfType<IMyTerminalBlock>(blocks, b => b.CustomName.Contains(oldString) && b.CustomName.Contains(filter));
	} else {
		GridTerminalSystem.GetBlocksOfType<IMyTerminalBlock>(blocks, b => b.CustomName.Contains(oldString));
	}

	int counter = 0;

	foreach (var block in blocks) {
		string oldName = block.CustomName;
		StringBuilder newName = new StringBuilder(oldName);

		counter++;

		newName.Replace(oldString, newString);

		sortList.Add(newName.ToString());

		result.Add(counter + ". " + oldName);
		result.Add("   -> " + newName);

		if (!testMode) block.CustomName = newName.ToString();
	}

	return counter;
}


// Remove
int Remove(string stringToRemove, string filter = "")
{
	// Create a new list and either get all the blocks or the ones, that match the name
	List<IMyTerminalBlock> blocks = new List<IMyTerminalBlock>();
	if (filter != "") {
		GridTerminalSystem.GetBlocksOfType<IMyTerminalBlock>(blocks, b => b.CustomName.Contains(filter) && b.CustomName.Contains(stringToRemove));
	} else {
		GridTerminalSystem.GetBlocksOfType<IMyTerminalBlock>(blocks, b => b.CustomName.Contains(stringToRemove));
	}

	int counter = 0;

	foreach (var block in blocks) {
		string oldName = block.CustomName;
		StringBuilder newName = new StringBuilder(oldName);

		counter++;

		newName.Replace(stringToRemove + " ", "").Replace(" " + stringToRemove, "").Replace(stringToRemove + spaceCharacter, "").Replace(spaceCharacter + stringToRemove, "");

		sortList.Add(newName.ToString());

		result.Add(counter + ". " + oldName);
		result.Add("   -> " + newName);

		if (!testMode) block.CustomName = newName.ToString();
	}

	return counter;
}


// Remove numbers
int RemoveNumbers(string filter = "")
{
	// Create a new list and either get all the blocks or the ones, that match the filter
	List<IMyTerminalBlock> blocks = new List<IMyTerminalBlock>();
	if (filter != "") {
		GridTerminalSystem.GetBlocksOfType<IMyTerminalBlock>(blocks, b => b.CustomName.Contains(filter));
	} else {
		GridTerminalSystem.GetBlocksOfType<IMyTerminalBlock>(blocks);
	}

	int counter = 0;

	foreach (var block in blocks) {
		string oldName = block.CustomName;
		StringBuilder newName = new StringBuilder(oldName);
		string number = GetNumberStr(oldName);

		counter++;

		if (number != String.Empty) {
			newName.Replace(GetNumberStr(oldName), "");
		}

		sortList.Add(newName.ToString());

		result.Add(counter + ". " + oldName);
		result.Add("   -> " + newName);

		if (!testMode) block.CustomName = newName.ToString();
	}

	return counter;
}


// Sort
int Sort(string filter, IMyCubeGrid grid = null, int counter = 0)
{
	// Create a new list and get filtered terminal blocks
	List<IMyTerminalBlock> blocks = new List<IMyTerminalBlock>();

	if (grid != null) {
		GridTerminalSystem.GetBlocksOfType<IMyTerminalBlock>(blocks, b => b.CustomName.Contains(filter) && b.CubeGrid == grid);
	} else {
		GridTerminalSystem.GetBlocksOfType<IMyTerminalBlock>(blocks, b => b.CustomName.Contains(filter));
	}

	int nameCounter = 0;
	int listCharAmount = blocks.Count.ToString().Length;

	// Sort list by name to keep rough order
	blocks.Sort((a, b) => GetNumber(a.CustomName).CompareTo(GetNumber(b.CustomName)));

	// Cycle through all the blocks
	foreach (var block in blocks) {
		counter++;
		nameCounter++;

		// Create a new name string
		string oldName = block.CustomName;
		string number = GetNumberStr(oldName);
		string newName = oldName;
		string newNumber = "";

		// Add zeros if needed
		if (blocks.Count > 1) {
			int counterCharAmount = nameCounter.ToString().Length;
			newNumber = spaceCharacter + StringRepeat('0', listCharAmount - counterCharAmount) + nameCounter;
		}

		if (number != String.Empty) {
			newName = newName.Replace(number, newNumber);
		} else {
			newName = newName + newNumber;
		}

		result.Add(counter + ". " + oldName);
		result.Add("   -> " + newName);

		if (!testMode) block.CustomName = newName.ToString();
	}

	return counter;
}


// Sortbygrid
int Sortbygrid(string filter)
{
	int counter = 0;

	HashSet<IMyCubeGrid> grids = new HashSet<IMyCubeGrid>();
	List<IMyTerminalBlock> blocks = new List<IMyTerminalBlock>();
	GridTerminalSystem.GetBlocksOfType<IMyTerminalBlock>(blocks, b => b.CustomName.Contains(filter));

	// Get all grids
	foreach (var block in blocks) {
		grids.Add(block.CubeGrid);
	}

	// Sort all grids
	foreach (var grid in grids) {
		if (counter != 0) result.Add("");
		result.Add("Grid: " + grid.CustomName + "\n--------------------------------------------------\n");
		counter = Sort(filter, grid, counter);
	}

	return counter;
}


// Autosort
int Autosort(string gridsToSort = "", int counter = 0)
{
	// Autosort the current grid only or all connected grids?
	if (gridsToSort != "") {
		HashSet<IMyCubeGrid> uniqueGrids = new HashSet<IMyCubeGrid>();
		List<IMyTerminalBlock> blocks = new List<IMyTerminalBlock>();
		GridTerminalSystem.GetBlocks(blocks);

		// Get all grids
		foreach (var block in blocks) {
			if (gridsToSort == "all") {
				uniqueGrids.Add(block.CubeGrid);
			} else {
				if (block.CubeGrid.CustomName.Contains(gridsToSort)) {
					uniqueGrids.Add(block.CubeGrid);
				}
			}
		}

		List<IMyCubeGrid> grids = new List<IMyCubeGrid>(uniqueGrids);

		// Sort all grids
		int i = gridIndex;
		if (i < grids.Count) {
			if (counter != 0) result.Add("");
			result.Add("Grid: " + grids[i].CustomName + "\n--------------------------------------------------\n");
			counter = AutosortRename(grids[i], counter);

			// Kill script and restart
			continueAutosort = true;
			autosortFilter = gridsToSort;
			autosortCounter = counter;
			gridIndex += 1;
			Runtime.UpdateFrequency = UpdateFrequency.Once;
			return 0;
		}
	} else {
		counter = AutosortRename(Me.CubeGrid, counter);
	}

	return counter;
}


// 2nd part of autosort - this gathers and renames the blocks of the selected grid
int AutosortRename(IMyCubeGrid grid, int counter)
{
	// Create a new list and get all terminal blocks
	List<IMyTerminalBlock> blocks = new List<IMyTerminalBlock>();
	List<string> trimmedNames = new List<string>();
	GridTerminalSystem.GetBlocksOfType<IMyTerminalBlock>(blocks, b => b.CubeGrid == grid);

	HashSet<String> uniqueNames = new HashSet<String>();

	foreach (var block in blocks) {
		// Check, if the given blockname is already in uniqueNames and add it if not
		string uniqueName = GetUniqueName(block.CustomName);
		trimmedNames.Add(uniqueName);
		uniqueNames.Add(uniqueName);
	}

	List<string> names = new List<string>(uniqueNames);
	names.Sort((a, b) => a.CompareTo(b));

	foreach (var name in names) {
		// Create a temporary list that contains all blocks of the current block's name
		List<IMyTerminalBlock> tempList = new List<IMyTerminalBlock>();

		for (int index = 0; index < trimmedNames.Count; index++) {
			if (trimmedNames[index] == name) {
				tempList.Add(blocks[index]);
			}
		}

		int tempCounter = 0;
		int listCharAmount = tempList.Count.ToString().Length;

		// Sort list by name to keep rough order
		tempList.Sort((a, b) => GetNumber(a.CustomName).CompareTo(GetNumber(b.CustomName)));

		foreach (var block in tempList) {
			counter++;
			tempCounter++;

			// Create a new name string
			string oldName = block.CustomName;
			string number = GetNumberStr(oldName);
			string newName = oldName;
			string newNumber = "";

			// Add zeros if needed
			if (tempList.Count > 1) {
				int counterCharAmount = tempCounter.ToString().Length;
				newNumber = spaceCharacter + StringRepeat('0', listCharAmount - counterCharAmount) + tempCounter;
			}

			if (number != String.Empty) {
				newName = newName.Replace(number, newNumber);
			} else {
				newName = newName + newNumber;
			}

			result.Add(counter + ". " + oldName);
			result.Add("   -> " + newName);

			if (!testMode) block.CustomName = newName;
		}
	}

	return counter;
}


// Add a string at front or back
int Add(string action, string stringToAdd, string filter = "")
{
	// Create a new list and either get all the blocks or the ones, that match the name
	List<IMyTerminalBlock> blocks = new List<IMyTerminalBlock>();
	if (action == "addfrontgrid" || action == "addbackgrid") {
		GridTerminalSystem.GetBlocksOfType<IMyTerminalBlock>(blocks, b => b.CubeGrid.CustomName.Contains(filter));
	} else if (filter != "") {
		GridTerminalSystem.GetBlocksOfType<IMyTerminalBlock>(blocks, b => b.CustomName.Contains(filter));
	} else {
		GridTerminalSystem.GetBlocks(blocks);
	}

	int counter = 0;

	foreach (var block in blocks) {
		StringBuilder newName = new StringBuilder();
		string oldName = block.CustomName;

		counter++;

		if (action.Contains("addfront")) {
			newName.Append(stringToAdd).Append(spaceCharacter).Append(oldName);
		}

		if (action.Contains("addback")) {
			newName.Append(oldName).Append(spaceCharacter).Append(stringToAdd);
		}

		sortList.Add(newName.ToString());

		result.Add(counter + ". " + oldName);
		result.Add("   -> " + newName);

		if (!testMode) block.CustomName = newName.ToString();
	}

	return counter;
}


// Rename a grid
int RenameGrid(string newName, string blockOnGrid = "!null")
{
	int counter = 0;

	if (blockOnGrid == "!null") {
		counter++;

		// Rename grid of the PB
		result.Add(counter + ". " + Me.CubeGrid.CustomName);
		result.Add("   -> " + newName);

		if (!testMode) Me.CubeGrid.CustomName = newName;
	} else {
		// Rename grid of specified block
		List<IMyTerminalBlock> blocks = new List<IMyTerminalBlock>();
		List<IMyCubeGrid> grids = new List<IMyCubeGrid>();
		GridTerminalSystem.GetBlocks(blocks);

		foreach (var block in blocks) {
			if (block.CustomName.Contains(blockOnGrid) && !grids.Contains(block.CubeGrid)) {
				counter++;
				grids.Add(block.CubeGrid);

				result.Add(counter + ". " + block.CubeGrid.CustomName);
				result.Add("   -> " + newName);

				if (!testMode) block.CubeGrid.CustomName = newName;
			}
		}
	}

	return counter;
}


// SortMode
void SortMode(List<string> listOfBlocks)
{
	if (listOfBlocks.Count == 0) {
		result.Add("Nothing to sort here..");
		return;
	}

	int counter = 0;
	int listCharAmount = listOfBlocks.Count.ToString().Length;

	// Sort list by name to keep rough order
	listOfBlocks.Sort((a, b) => GetNumber(a).CompareTo(GetNumber(b)));

	// Cycle through all the blocks
	foreach (var blockName in listOfBlocks) {
		counter++;

		// Create a new name string
		string oldName = blockName;
		string number = GetNumberStr(oldName);
		string newName = oldName;
		string newNumber = "";

		// Add zeros if needed
		if (listOfBlocks.Count > 1) {
			int counterCharAmount = counter.ToString().Length;
			newNumber = spaceCharacter + StringRepeat('0', listCharAmount - counterCharAmount) + counter;
		}

		if (number != String.Empty) {
			newName = newName.Replace(number, newNumber);
		} else {
			newName = newName + newNumber;
		}

		result.Add(counter + ". " + oldName);
		result.Add("   -> " + newName);

		if (!testMode) {
			IMyTerminalBlock block = GridTerminalSystem.GetBlockWithName(blockName);
			if (block != null) block.CustomName = newName;
		}
	}
}


// Result
void ShowResult(int counter, bool grid = false)
{
	if (!showHelp) {
		string item = "blocks";

		if (grid) {
			item = "grids";
		}

		if (counter == 0) result.Add("No " + item + " found!");

		if (testMode) {
			result.Add("\nTest Mode. No " + item + " were renamed!");
		} else {
			result.Add("\nRenamed " + counter + " " + item + "!");
		}
	}

	result.Add("\nHint:\nCan't see the whole list?\nLook in the custom data to view it all!");

	string text = String.Join("\n", result);
	Me.CustomData = text;

	if (result.Count > 200) {
		for (int i = 0; i <= 99; i++) {
			Echo(result[i]);
		}

		Echo(".\n.\n.");

		for (int i = result.Count - 100; i < result.Count; i++) {
			Echo(result[i]);
		}
	} else {
		Echo(text);
	}
}


// String repeater
string StringRepeat(char charToRepeat, int numberOfRepetitions)
{
	if (numberOfRepetitions <= 0) {
		return "";
	}
	return new string(charToRepeat, numberOfRepetitions);
}


// Get number as string
string GetNumberStr(string input)
{
	return System.Text.RegularExpressions.Regex.Match(input, pattern).Value;
}


// Get number as int
int GetNumber(string input)
{
	int number = 0;
	int.TryParse(System.Text.RegularExpressions.Regex.Match(input, pattern).Value.TrimStart(' ').TrimStart(spaceCharacter), out number);
	return number;
}


// Get unique name
string GetUniqueName(string input)
{
	return System.Text.RegularExpressions.Regex.Match(input, @"\D+").Value.TrimEnd(' ').TrimEnd(spaceCharacter);
}


// Help
void Help(string topic = "")
{
	bool topicExists = false;

	if (topic == "") {
		result.Add("Instructions:\n");
		topicExists = true;
	} else {
		result.Add("Usage:\n");
	}

	if (topic == "" || topic == "rename") {
		result.Add("--- Rename ---");
		result.Add("Rename a block found with the filter:");
		result.Add("rename,BLOCKFILTER,NEWNAME\n");
		topicExists = true;
	}

	if (topic == "" || topic == "replace") {
		result.Add("--- Replace ---");
		result.Add("Replace a string with another one:");
		result.Add("replace,OLDSTRING,NEWSTRING\n");
		topicExists = true;
	}

	if (topic == "" || topic == "remove") {
		result.Add("--- Remove ---");
		result.Add("Remove a string:");
		result.Add("remove,STRING[,BLOCKFILTER]\n");
		topicExists = true;
	}

	if (topic == "" || topic == "removenumbers") {
		result.Add("--- Remove Numbers ---");
		result.Add("Remove numbers from blocknames:");
		result.Add("removenumbers[,BLOCKFILTER]\n");
		topicExists = true;
	}

	if (topic == "" || topic == "sort") {
		result.Add("--- Sort ---");
		result.Add("Create new continuous numbers:");
		result.Add("sort,BLOCKFILTER\n");
		result.Add("Create new numbers based on the grid:");
		result.Add("sortbygrid,BLOCKFILTER\n");
		topicExists = true;
	}

	if (topic == "" || topic == "autosort") {
		result.Add("--- Autosort ---");
		result.Add("Autosort all blocks on your grid with automatic numbers:");
		result.Add("autosort\n");
		result.Add("Autosort all blocks on a specific grid:");
		result.Add("autosort,GRIDNAME\n");
		result.Add("Autosort every connected grid:");
		result.Add("autosort,all\n");
		topicExists = true;
	}

	if (topic == "" || topic == "addfront" || topic == "addback") {
		result.Add("--- Add strings ---");
		result.Add("Add a string at the front or back:");
		result.Add("addfront,STRING[,BLOCKFILTER]");
		result.Add("addback,STRING[,BLOCKFILTER]\n");
		topicExists = true;
	}

	if (topic == "" || topic == "addfrontgrid" || topic == "addbackgrid") {
		result.Add("--- Add strings whole grid ---");
		result.Add("Add a string at the front or back of every blockname on a grid:");
		result.Add("addfrontgrid,STRING,GRIDNAME");
		result.Add("addbackgrid,STRING,GRIDNAME\n");
		topicExists = true;
	}

	if (topic == "" || topic == "renamegrid") {
		result.Add("--- Rename grid ---");
		result.Add("Rename a grid:");
		result.Add("renamegrid,NEWNAME[,BLOCKONGRID]\n");
		topicExists = true;
	}

	if (!topicExists) {
		result.Add("--- Error ---");
		result.Add("No topic with the given name exists!");
	}

	if (topicExists) {
		result.Add("Additional parameters:");
		result.Add("Test before renaming:\n" + testKeyword);
		result.Add("Sort after renaming:\n" + sortKeyword);
	}

	showHelp = true;
}